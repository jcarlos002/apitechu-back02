package com.techu.backend.service;

import com.techu.backend.model.ProductoModel;
import com.techu.backend.repository.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service    // equivalente a @component para indicar la clase que da servicio servicio
public class ProductoMongoService {

    @Autowired  //Indica que se cree al arrancar la aplicacion
    ProductoRepository productoRepository;

    // READ
    public List<ProductoModel> findAll(){
        return productoRepository.findAll();
    }

    // CREATE
    public ProductoModel save(ProductoModel newProducto){
        return productoRepository.save(newProducto);

    }
    // READ ID
    public Optional<ProductoModel> findById(String id){
        return productoRepository.findById(id);
    }
    // DELETE
    public boolean delete(ProductoModel productoModel){
        try {
            productoRepository.delete(productoModel);
            return true;
        } catch(Exception ex){
            return false;
        }
    }

}
