package com.techu.backend.controller;

import com.techu.backend.model.ProductoModel;
import com.techu.backend.service.ProductoMongoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("${url.base}")
public class ProductoController {

    @Autowired
    private ProductoMongoService productoService;

    @GetMapping("")
    public String home()  {
        return "API REST Tech U! v2.0.0";
    }

    // GET todos los productos (collection)
    @GetMapping("/productos")
    public List<ProductoModel> getProductos() {
        return productoService.findAll();
    }

    // POST para crear productos
    @PostMapping("/productos")
    public ProductoModel postProducto(@RequestBody ProductoModel newProduct) {

        productoService.save(newProduct);
        return newProduct;
    }

    // GET a un producto por Id (instancia)
    @GetMapping("/productos/{id}")
    // Si en responseEntity se informa ResponseEntity<ProductoModel> solo se podrian devolver variables de ese tipo
    public Optional<ProductoModel> getProductoById(@PathVariable String id){
       return productoService.findById(id);
    }
    // PUT a un producto (si existe lo reemplaza)
    @PutMapping("/productos")
    public void putProducto(@RequestBody ProductoModel productoToUpdate){
        productoService.save(productoToUpdate);
    }

    // DELETE para borrar un producto
    @DeleteMapping("/productos")
    public boolean  deleteProducto(@RequestBody ProductoModel productoToDelete) {
        return productoService.delete(productoToDelete);
    }

}
